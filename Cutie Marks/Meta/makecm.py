#!/usr/bin/env python3
from math import sqrt
s12 = (sqrt(3)-1) / sqrt(8)
c12 = (sqrt(3)+1) / sqrt(8)

def p(x, y):
    cx, cy = x*c12+y*s12, x*s12+y*c12
    return f"{cx} {-cy}"

with open("cm.svg", 'w') as f:
    print('<svg width="720" height="720" viewBox="-12 -12 24 24" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">', file=f)
    print(f'<defs><path d="M{p(3,-2)}C{p(4,0)} {p(5,4)} {p(-2,12)}L{p(3,9)}C{p(7,3)} {p(6,-1)} {p(4,-3)}Z" id="A"/></defs>', file=f)
    print(f'  <use xlink:href="#A" fill="#5a318e"/>', file=f)
    print(f'  <use xlink:href="#A" transform="rotate(60)" fill="#54a2e1"/>', file=f)
    print(f'  <use xlink:href="#A" transform="rotate(120)" fill="#5a318e"/>', file=f)
    print(f'  <use xlink:href="#A" transform="scale(-1)" fill="#54a2e1"/>', file=f)
    print(f'  <use xlink:href="#A" transform="rotate(240)" fill="#5a318e"/>', file=f)
    print(f'  <use xlink:href="#A" transform="rotate(-60)" fill="#54a2e1"/>', file=f)
    print(f'  <path d="M{p(5,1)} {p(-1,6)} {p(-6,5)} {p(-5,-1)} {p(1,-6)} {p(6,-5)}Z" fill="none" stroke="#576ab7" stroke-width=".5"/>', file=f)
    print('</svg>', file=f)
