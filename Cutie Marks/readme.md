This sub-folder includes all cutie marks I have done for any purpose – new pony, curiosity, gift for someone else, the likes – arranged according to their holders' roles in the show.

All cutie marks are in the orientation that would be seen if the pony is facing right, except those for the mane six, Shining Armour and Big Macintosh which are for a pony facing left. Separate files are available for Sweetie Belle's cutie mark, which differs between flanks.

The colours are [MLP Vector Club recommendations](https://mlpvc-rr.ml/cg), with missing values taken from [here](https://i.imgur.com/bHwUS7R.png) and canon screenncaps. Some cutie marks may not appear as intended in other SVG viewers like web browsers due to my practice of leaving out XML declarations, but all should open correctly in Inkscape, which puts them in when needed.
